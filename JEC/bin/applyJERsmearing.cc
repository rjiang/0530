#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <limits>
#include <cassert>
#include <filesystem>

#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TH2.h>
#include <TDirectory.h>

#include "Core/CommonTools/interface/toolbox.h"

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/JEC/interface/Resolution.h"
#include "Core/JEC/interface/matching.h"

#include "Core/CommonTools/interface/ControlPlots.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Get JER smearing corrections in the form of factors to store in the `scales`
/// member of `RecJet`.
void applyJERsmearing
            (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
             const fs::path& output, //!< name of output ROOT files (n-tuples)
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    auto R = metainfo.Get<int>("flags", "R");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );

    GenEvent * genEvt = nullptr;
    RecEvent * recEvt = nullptr;
    PileUp * pu = nullptr;
    tIn->SetBranchAddress("genEvent", &genEvt);
    tIn->SetBranchAddress("recEvent", &recEvt);
    tIn->SetBranchAddress("pileup", &pu);

    vector<GenJet> * genJets = nullptr;
    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("genJets", &genJets);
    tIn->SetBranchAddress("recJets", &recJets);

    const auto& JERtables = config.get<fs::path>("corrections.JER.tables");
    const auto& mode = config.get<string>("corrections.JER.mode");

    if (mode != "stochasticOnly" && mode != "scalingAllMatched" && mode != "scalingCoreOnly"  ) 
        BOOST_THROW_EXCEPTION( invalid_argument("`" + mode + "` is not recognised.") );

    metainfo.Set<fs::path>("corrections", "JER", "tables", JERtables);
    metainfo.Set<string  >("corrections", "JER", "mode"  , mode     );

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal") };

    bool applySyst = (steering & DT::syst) == DT::syst;
    if (applySyst) {
        metainfo.Set<string>("variations", RecJet::ScaleVar, "JER" + SysDown);
        metainfo.Set<string>("variations", RecJet::ScaleVar, "JER" + SysUp);

        calib.push_back(ControlPlots("JER" + SysDown));
        calib.push_back(ControlPlots("JER" + SysUp));
    }

    if (!fs::exists(JERtables))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Directory does not exists.",
                               JERtables, make_error_code(errc::no_such_file_or_directory)) );
    Matching<RecJet, GenJet>::maxDR = (R/10.)/2;
    JetEnergy::Resolution jer(JERtables, R, metainfo.Seed<2948754>(slice), applySyst);

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        raw(*genJets, genEvt->weights.front());
        raw(*recJets, genEvt->weights.front() * recEvt->weights.front());

        static const bool CoreOnly = mode == "scalingCoreOnly";
        Matching<RecJet, GenJet> matching(*recJets, CoreOnly, pu->rho); // the rec jet are here copied
        recJets->clear(); // will be filled again in the loop

        // smear matched jet with scaling method (if allowed by the mode)
        static const bool StochasticOnly = mode == "stochasticOnly";
        if (!StochasticOnly)
        for (const GenJet& genJet: *genJets) {
            auto recJet = matching.HighestPt(genJet);

            // if no successful match
            if (recJet.p4.Pt() > 14000 /* GeV */) continue;

            recJet.scales = jer.ScalingMethod(recJet, genJet, pu->rho);
            recJets->push_back(recJet);
        }

        // smear unmatched jets with stochastic method
        for (RecJet& recJet: matching.mcands) {
            recJet.scales = jer.StochasticMethod(recJet, pu->rho);
            recJets->push_back(recJet);
        }

        sort(recJets->begin(), recJets->end(), pt_sort);

        for (size_t i = 0; i < calib.size(); ++i) {
            calib.at(i)(*genJets, genEvt->weights.front());
            calib.at(i)(*recJets, genEvt->weights.front() * recEvt->weights.front(), i);
        }

        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();
    raw.Write(fOut.get());
    for (auto& c: calib)
        c.Write(fOut.get());

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Apply jet energy smearing correction. In practice, the correction factors "
                            "are stored in a dedicated vector in the reconstructed jets.\n"
                            "Notes:\n"
                            " - `stochasticOnly` means stochastic smearing regardless of matching;\n"
                            " - `scalingAllMatched` means stochastic only if there is no matching;\n"
                            " - `scalingCoreOnly` means scaling only in the core of the resolution.",
                            DT::config | DT::split | DT::fill | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("JERtables", "corrections.JER.tables", "directory to tables provided by JetMET "
                                                                     "(in txt format)")
               .arg<string>("mode", "corrections.JER.mode", "stochasticOnly, scalingAllMatched, scalingCoreOnly");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::applyJERsmearing(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

#ifndef _matchingJM_h_
#define _matchingJM_h_

#include "Core/Objects/interface/Jet.h"

#include <iostream>
#include <utility>
#include <vector>
#include <map>

#include "Math/VectorUtil.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Function that performs matching between an input list of reconstructed level
/// jets and a list of generator level jets, based on their angular distance.
/// The JetMET matching algorithm is used for the procedure.
///
/// JetMET matching algorithm (to what we understand):
/// (_1_) Allow all possible pairs between gen and rec jets
/// (_2_) Discard pairs with DR larger than R/2
/// (_3_) Sort by ascending DR (smaller to larger)
/// (_4_) Keep pais that are uniquely defined
///
/// According to a page on the [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetResolution),
/// the matching radius should be taken as half of the cone size radius...
std::map<float, std::pair<RecJet, GenJet>> match (const std::vector<RecJet> recjets, //!< vector of rec jet list
                                                  const std::vector<GenJet> genjets, //!< vector of gen jet list
                                                  const float maxDR //!< max angular distance
                                                  )
{
    // Make pairs (_1_) and (_3_)
    std::map<float, std::pair<RecJet, GenJet>> couples;
    for (const auto& recJet: recjets)
    for (const auto& genJet: genjets) {
        auto DR = DeltaR(recJet.p4, genJet.p4);
        if (DR >= maxDR) continue;
        couples.insert( std::make_pair(DR, std::make_pair(recJet, genJet)) );
    }

    // Remove doublets at larger DeltaR (_3_) and (_4_)
    for (auto rit = couples.rbegin(); rit != couples.rend();) {
        bool del = false;
        auto current = couples.find( rit->first );
        for (auto it = couples.begin(); it != current && !del; ++it)
            del |= rit->second. first.p4.Pt() == it->second. first.p4.Pt() || //TODO is it fine if we use p4.Pt() instead of CorrPt() ??
                   rit->second.second.p4.Pt() == it->second.second.p4.Pt();
        if (del) couples.erase(current);
        else     ++rit;
    }

    return couples;
}

} // end of namespace

#endif

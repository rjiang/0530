#!/bin/zsh
set -e
set -x

folder="$1"
input_file="$(readlink -f "$2")"
isMC="$3"

mkdir -p "$folder"
cd "$folder"

if [[ ! -d $DARWIN_TABLES ]]
then
    export DARWIN_TABLES=$CMSSW_BASE/test/tables
    git clone https://gitlab.cern.ch/DasAnalysisSystem/tables.git $DARWIN_TABLES
fi

# diff $CMSSW_BASE/python/Core/Ntupliser/Ntupliser_cfg.py $CMSSW_BASE/src/Core/Ntupliser/python/Ntupliser_cfg.py (TODO)
cp $CMSSW_BASE/src/Core/Ntupliser/test/CI.json $CMSSW_BASE/test/CI.json
cmsRun $CMSSW_BASE/src/Core/Ntupliser/python/Ntupliser_cfg.py inputFiles=file:"$input_file" outputFile=ntuple.root configFile=$CMSSW_BASE/test/CI.json
mergeNtuples ntuple.root mergeNtuples.root /dev/null false -f

# MET
applyMETfilters mergeNtuples.root applyMETfilters.root
mergeNtuples ntuple.root mergeNtuples.root /dev/null true -f
# TODO getMETfraction

# jet veto maps
vetomap=$DARWIN_TABLES/jetvetomaps/eff_map_UL16.root
if [[ ! -f $vetomap ]]; then
    echo Getting the maps
    (cd $DARWIN_TABLES/jetvetomaps && ./getULRun2Maps)
fi
applyConservativeVetoMap mergeNtuples.root applyConservativeVetoMap.root $vetomap
mergeNtuples ntuple.root mergeNtuples.root $vetomap true -f

previous_file=mergeNtuples.root

if [ "$isMC" = "yes" ]; then
    # normalisation
    getSumWeights mergeNtuples.root getSumWeights.root 0
    applyMClumi mergeNtuples.root applyMClumi.root 1 -f

    # PU staub
    applyPUcleaning applyMClumi.root applyPUcleaning.root /dev/null -f

    # jet energy corrections
    JES=$DARWIN_TABLES/JES/Summer19UL16_V7_MC
    if [[ ! -d $JES ]]; then
        echo Untarring the JES tables
        (cd $DARWIN_TABLES/JES && tar xf Summer19UL.tar.gz)
    fi
    applyJEScorrections applyPUcleaning.root applyJEScorrections.root $JES -f

    # jet energy resolution
    getJetResponse applyJEScorrections.root getJetResponse.root false
    fitJetResponse getJetResponse.root fitJetResponse.root
    fitJetResolution fitJetResponse.root fitJetResolution.root
    JER=$DARWIN_TABLES/JER/Summer20UL16_JRV3_MC
    if [[ ! -d $JER ]]; then
        echo Untarring the JER tables
        (cd $DARWIN_TABLES/JER && tar xf Summer19-20UL.tar.gz)
    fi
    applyJERsmearing applyJEScorrections.root applyJERsmearing.root $JER stochasticOnly -f

    # PU profile
    triggers=$DARWIN_TABLES/triggers/thresholds/2018/HLT_AK8PFJet.info
    getPUprofile applyJERsmearing.root getPUprofile.root $triggers
    applyPUprofCorrection applyJERsmearing.root applyPUprofCorrection.root getPUprofile.root getPUprofile.root 4 -f

    previous_file=applyPUprofCorrection.root
fi

# MN observables
getMNobservables $previous_file getMNobservables.root

# muons
applyRochesterCorr $previous_file applyRochesterCorr.root $DARWIN_TABLES/Rochester/RoccoR2016aUL.txt false -f
muonEff=$DARWIN_TABLES/Muons/2016_preVFP/Efficiency_muon_generalTracks_Run2016preVFP_UL_trackerMuon.root
applyMuonEffCorr applyRochesterCorr.root applyMuonEffCorr.root $muonEff:NUM_TrackerMuons_DEN_genTracks -f
getDimuonSpectrum applyMuonEffCorr.root getDimuonSpectrum.root
previous_file=applyMuonEffCorr.root

#if [ "$isMC" = "yes" ]; then # TODO
#    wget -nc https://gitlab.cern.ch/DasAnalysisSystem/tables/-/raw/master/triggers/muons/ScaleFactor_DoubleMuonTriggers_UL2018.root
#    applyDimuonTriggerStrategy applyMuonEffCorr.root applyDimuonTriggerStrategy.root ScaleFactor_DoubleMuonTriggers_UL2018.root:ScaleFactorTight_UL2018 -f
#    previous_file=applyDimuonTriggerStrategy.root
#fi

# meta information
getMetaInfo $previous_file meta.info
cat meta.info

# reproducibility
mergeNtuples ntuple.root mergeNtuples.root -c meta.info -f
previous_file=mergeNtuples.root
if [ "$isMC" = "yes" ]; then
    applyMClumi mergeNtuples.root applyMClumi.root -c meta.info -f
    applyPUcleaning applyMClumi.root applyPUcleaning.root -c meta.info -f
    applyJEScorrections applyPUcleaning.root applyJEScorrections.root -c meta.info -f
    applyJERsmearing applyJEScorrections.root applyJERsmearing.root -c meta.info -f
    applyPUprofCorrection applyJERsmearing.root applyPUprofCorrection.root -c meta.info -f
    previous_file=applyPUprofCorrection.root
fi
applyRochesterCorr $previous_file applyRochesterCorr.root -c meta.info -f
applyMuonEffCorr applyRochesterCorr.root applyMuonEffCorr.root -c meta.info -f
previous_file=applyMuonEffCorr.root
#if [ "$isMC" = "yes" ]; then # TODO
#    applyDimuonTriggerStrategy applyMuonEffCorr.root applyDimuonTriggerStrategy.root -c meta.info -f
#    previous_file=applyDimuonTriggerStrategy.root
#fi
getMetaInfo $previous_file meta2.info
diff meta.info meta2.info

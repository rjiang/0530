#!/bin/zsh
set -e

cfg=$CMSSW_BASE/python/Core/Ntupliser/Ntupliser_cfg.py

python $cfg

for configFile in `ls $CMSSW_BASE/src/Core/Ntupliser/test/*.json`
do
    echo Testing $configFile
    python $cfg configFile=$configFile
    mkNtuples $configFile
done

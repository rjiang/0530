# Objects

Unless one focuses on one single-count observable, an event will provide more than one entry.
Hence two groups of structures have been defined in `interface` (for the headers) and `src` (for the source).
 - Event objects, containing all information common to all entries, such as the pile-up or the MET.
 - Physics objects (jets, photons, etc.), containing all information relative to one single entry.

All structures contain a `clear()` method for easier use in the *n*-tupliser.
One particular feature related to the current framework is the presence of vectors of *weights* and of *scales*, which allow the storage of the corrections and all their variations.

## Physics objects

The `FourVector` is a typedef of an existing type.
The `PhysicsObject` type contains all generic information for particles and jets, such as the four-momentum and the scale and weight vectors.
Only high-level objects such as jets, photons, or muons are currently considered.
The minimum amount of information is kept to prevent from too heavy events.

## Event objects

Several structures are defined to contain the information of the event:
 - `*Event` for general information such as run number or hard scale.
 - `Trigger` contains the bits corresponding to the fired triggers and the prescales.
 - `MET` contains the type-I MET.
 - `PileUp` contains information relative to the measurement and the simulation of the pileup.
 - `PrimaryVertex ` contains information only necessary for the event selection.
